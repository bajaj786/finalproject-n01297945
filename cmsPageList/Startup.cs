﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(cmsPageList.Startup))]
namespace cmsPageList
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
