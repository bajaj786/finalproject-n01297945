﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddNew.aspx.cs" Inherits="cmsPageList.AddNew" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <br />
    <br />
    <table>
        <tr>
            <td>
                <asp:Label ID="lblMsg" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="Page Name" ID="lblName" runat="server" /></td>
            <td>
                <asp:TextBox ID="txtname" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="Content" ID="lblContent" runat="server" /></td>
            <td>
                <asp:TextBox ID="txtContent" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnAdd" Text="Add" runat="server" OnClick="btnAdd_Click" /></td>
        </tr>

    </table>
</asp:Content>
