﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cmsPageList
{
    public partial class AddNew : System.Web.UI.Page
    {
        string query_string = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=G:\Web application development\FINAL PROJECT\cmsPageList\cmsPageList\cmsPageList\App_Data\dbPage.mdf;Integrated Security = True";
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(query_string);
            con.Open();
            //command taken from https://www.dotnetperls.com/sqlcommand-->
            if (txtname.Text != "" && txtContent.Text != "")
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO page (page_title,page_content) VALUES (@page_title,@page_content)", con);
                cmd.Parameters.AddWithValue("@page_title", (txtname.Text.ToString()));
                cmd.Parameters.AddWithValue("@page_content", (txtContent.Text.ToString()));
                cmd.ExecuteNonQuery();
                con.Close();
                Response.Redirect("Default.aspx");
            }
            else
            {
                lblMsg.Text = "Please enter all detail";
            }
        }
    }
}