﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cmsPageList
{
    public partial class _Default : Page
    {
        string query_string = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=G:\Web application development\FINAL PROJECT\cmsPageList\cmsPageList\cmsPageList\App_Data\dbPage.mdf;Integrated Security = True";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["pageid"] == null)
            {
                gvPageGridview.Visible = true;
                PlaceHolder1.Visible = true;
                lblID.Visible = false;
                lblName.Visible = false;
                lblContent.Visible = false;
                btnAdd.Visible = true;
            }
            else
            {
                gvPageGridview.Visible = false;
                PlaceHolder1.Visible = false;
                btnAdd.Visible = false;

                lblID.Visible = true;
                lblName.Visible = true;
                lblContent.Visible = true;

                lblID.Text = "ID : " + Request.QueryString["pageid"];
                lblName.Text = "Name : " + Request.QueryString["name"];
                lblContent.Text = "Content : " + Request.QueryString["content"];
            }

            if (!this.IsPostBack)
            {
                setGridviewPageList();
            }
        }

        private void setGridviewPageList()
        {
            DataTable dtpageTable = fillDataTable();

            StringBuilder menuPlaceHolderString = new StringBuilder();

            menuPlaceHolderString.Append("<table>");
            menuPlaceHolderString.Append("<tr>");

            foreach (DataRow row in dtpageTable.Rows)
            {
                menuPlaceHolderString.Append("<td><a href='Default.aspx?pageid=" + row[0] + "&name=" + row[1] + "&content=" + row[2] + "'>");
                menuPlaceHolderString.Append(row[1]);
                menuPlaceHolderString.Append("</a> &nbsp &nbsp &nbsp</td>");
            }
            menuPlaceHolderString.Append("</tr>");
            menuPlaceHolderString.Append("</table>");

            PlaceHolder1.Controls.Add(new Literal { Text = menuPlaceHolderString.ToString() });

            if (dtpageTable.Rows.Count > 0)
            {
                gvPageGridview.DataSource = dtpageTable;
                gvPageGridview.DataBind();
            }
            else
            {
                dtpageTable.Rows.Add(dtpageTable.NewRow());
                gvPageGridview.DataSource = dtpageTable;
                gvPageGridview.DataBind();

                gvPageGridview.Rows[0].Cells.Clear();
                gvPageGridview.Rows[0].Cells.Add(new TableCell());
                gvPageGridview.Rows[0].Cells[0].ColumnSpan = dtpageTable.Columns.Count;
                gvPageGridview.Rows[0].Cells[0].Text = "No Data Found ..!";
                gvPageGridview.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            }
        }

        public DataTable fillDataTable()
        {
            using (SqlConnection sqlConObj = new SqlConnection(query_string))
            {
                using (SqlCommand sqlCmdObj = new SqlCommand("SELECT page_id, page_title, page_content FROM page"))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        sqlCmdObj.Connection = sqlConObj;
                        sda.SelectCommand = sqlCmdObj;
                        using (DataTable dtObject = new DataTable())
                        {
                            sda.Fill(dtObject);
                            return dtObject;
                        }
                    }
                }
            }
        }

        protected void gvPageGridview_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvPageGridview.EditIndex = e.NewEditIndex;
            setGridviewPageList();
        }

        protected void gvPageGridview_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvPageGridview.EditIndex = -1;
            setGridviewPageList();
        }

        protected void gvPageGridview_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            using (SqlConnection sqlConObj = new SqlConnection(query_string))
            {
                sqlConObj.Open();

                SqlCommand sqlCmdObj = new SqlCommand("UPDATE page SET page_title=@page_title,page_content=@page_content WHERE page_id =@page_id", sqlConObj);
                sqlCmdObj.Parameters.AddWithValue("@page_title", (gvPageGridview.Rows[e.RowIndex].FindControl("page_Title") as TextBox).Text.Trim());
                sqlCmdObj.Parameters.AddWithValue("@page_content", (gvPageGridview.Rows[e.RowIndex].FindControl("page_Content") as TextBox).Text.Trim());
                sqlCmdObj.Parameters.AddWithValue("@page_id", Convert.ToInt32(gvPageGridview.DataKeys[e.RowIndex].Value.ToString()));

                sqlCmdObj.ExecuteNonQuery();

                gvPageGridview.EditIndex = -1;
                setGridviewPageList();

                sqlConObj.Close();
            }
        }

        protected void gvPageGridview_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            using (SqlConnection sqlConObj = new SqlConnection(query_string))
            {
                sqlConObj.Open();

                SqlCommand commandObject = new SqlCommand("DELETE FROM page WHERE page_id =@page_id", sqlConObj);
                commandObject.Parameters.AddWithValue("@page_id", Convert.ToInt32(gvPageGridview.DataKeys[e.RowIndex].Value.ToString()));
                commandObject.ExecuteNonQuery();

                setGridviewPageList();
                sqlConObj.Close();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddNew.aspx");
        }
    }
}