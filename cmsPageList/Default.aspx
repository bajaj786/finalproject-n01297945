﻿<%@ Page Title="CRUDFinalProject" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="cmsPageList._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <br />
    <br />

    <asp:Label ID="lblID" runat="server" />
    <br />
    <asp:Label ID="lblName" runat="server" />
    <br />
    <asp:Label ID="lblContent" runat="server" />

    <br />

    <asp:PlaceHolder ID="PlaceHolder1" runat="server" />
    <br />

    <asp:Button ID="btnAdd" Text="Add New Page" runat="server" OnClick="btnAdd_Click" />

    <br />
    <br />
    <!--took idea from this site https://www.fastlearning.in/aspnet/aspnet_linkbutton_control.php -->
    <asp:GridView ID="gvPageGridview" runat="server" CellPadding="4"
        AutoGenerateColumns="False" ShowFooter="false" DataKeyNames="page_id"
        OnRowEditing="gvPageGridview_RowEditing" OnRowCancelingEdit="gvPageGridview_RowCancelingEdit"
        OnRowUpdating="gvPageGridview_RowUpdating"
        OnRowDeleting="gvPageGridview_RowDeleting"
        BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px">


        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
        <RowStyle BackColor="White" ForeColor="#330099" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
        <SortedAscendingCellStyle BackColor="#FEFCEB" />
        <SortedAscendingHeaderStyle BackColor="#AF0101" />
        <SortedDescendingCellStyle BackColor="#F6F0C0" />
        <SortedDescendingHeaderStyle BackColor="#7E0000" />
         
        <Columns>

            <asp:TemplateField HeaderText="Title">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("page_title") %>' runat="server"></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="page_Title" Text='<%# Eval("page_title") %>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="page_TitleFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Content">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("page_content") %>' runat="server"></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="page_Content" Text='<%# Eval("page_content") %>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="page_ContentFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>
           
            <asp:TemplateField HeaderText="Operation">
                <ItemTemplate>
                    <asp:LinkButton Text="Edit" runat="server" CommandName="Edit" ToolTip="Edit" Width="50px" Height="20px" />
                    <asp:LinkButton Text="Delete" runat="server" CommandName="Delete" ToolTip="Delete" Width="50px" Height="20px" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:LinkButton Text="Save" runat="server" CommandName="Update" ToolTip="Update" Width="50px" Height="20px" />
                    <asp:LinkButton Text="Cancel" runat="server" CommandName="Cancel" ToolTip="Cancel" Width="50px" Height="20px" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:LinkButton Text="Add" runat="server" CommandName="AddNew" ToolTip="Add New" Width="50px" Height="20px" />
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
